var fs = require('fs'),
    ss = require('simple-statistics'),
    glob = require('glob');

/**
  * Return file size in bytes
  */
function getFilesizeInBytes(filename) {

  var stats = fs.statSync(filename),
      fileSizeInBytes = stats.size;

  return fileSizeInBytes;

}

function getFileSizeInKilobytes(f) {
  return getFilesizeInBytes(f) / 1000;
}



function computeDifferenceForAllQulities() {

  var qualities = [60, 70, 75, 80, 90],
      type = ['full', 'big', 'thumb'];

  type.forEach(function(type) {

    console.log('\nResults for type = "' + type + '"');

    qualities.forEach(function(quality) {
      computeDifferenceForQuality(type, quality)
    });

  });

}

function computeDifferenceForQuality(type, quality) {

  var originalSizes = [],
    compressedSizes = [],
    diffSizes = [],
    files;

  files = glob.sync('./images/' + type + '_uncompressed_' + quality + '/*.jpg');

  files.forEach(function(filename, i) {

    originalSizes.push(getFileSizeInKilobytes(filename));

    compressedSizes.push(getFileSizeInKilobytes(filename.replace('_uncompressed', '')));

    diffSizes.push((originalSizes[i] - compressedSizes[i]) / originalSizes[i] * 100);

  });

  console.log('quality = ' + quality);
  console.log(ss.mean(diffSizes).toFixed(2) + ' ± ' + ss.standard_deviation(diffSizes).toFixed(2) + ' %');

}

computeDifferenceForAllQulities();
