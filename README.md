===HOW TO USE

```bash
# Download images to ./images
node grab_images.js

# Optimize images
ruby optimize_advert_images.rb <quality>

# Calc stats for different type of optimizations (default 60, 70, 75, 80, 90)
node compare_optimizations.js
```


For example
```bash
npm install
node grab_images.js
ruby optimize_advert_images.rb 60
ruby optimize_advert_images.rb 70
ruby optimize_advert_images.rb 75
ruby optimize_advert_images.rb 80
ruby optimize_advert_images.rb 90
node compare_optimizations.js
```
