# full_uncompressed_60 = original
# full_60 = optimized to 60 original
# big_uncompressed_60 = downscaled and cropped to 233*160
# big_60 = downscaled and cropped to 233*160, optimized to 60
# thumb_uncompressed_60 = downscaled and cropped to 130*130
# thumb_uncompressed_60 = downscaled and cropped to 130*130, optimized to 60

require 'find'
require 'RMagick'
include Magick
require 'pry'
require 'fileutils'
require 'pathname'

def command_path(command)
  command
end

def check_folder(folder_name)
  unless File.directory?("#{@directory}/{folder_name}")
    FileUtils.mkdir_p("#{@directory}/#{folder_name}")
    puts "directory created: #{@directory}/#{folder_name}"
  end
end

def create_resized_version(path, file, width, height)
  img = Magick::Image.read(file).first
  resized = img.resize_to_fit(width, height)
  resized.write(path)
  GC.start
end

def create_big_version(file)
  path = "#{@directory}/big_#{@quality}/#{File.basename(file)}"
  create_resized_version(path, file, 233, 160)
end

def create_big_uncompressed_version(file)
  path = "#{@directory}/big_uncompressed_#{@quality}/#{File.basename(file)}"
  create_resized_version(path, file, 233, 160)
end

def create_thumb_version(file)
  path = "#{@directory}/thumb_#{@quality}/#{File.basename(file)}"
  create_resized_version(path, file, 130, 130)
end

def create_thumb_uncompressed_version(file)
  path = "#{@directory}/thumb_uncompressed_#{@quality}/#{File.basename(file)}"
  create_resized_version(path, file, 130, 130)
end

def _create_uncompressed_versions(file)
  create_big_uncompressed_version(file)
  create_thumb_uncompressed_version(file)
end

def _create_versions(file)
  create_big_version(file)
  create_thumb_version(file)
end

def _create_full(file)
  # we assume that all images are horizontal, so we use only resize_to_fill
  path = "#{@directory}/full_#{@quality}/#{File.basename(file)}"
  path_uncompressed = "#{@directory}/full_uncompressed_#{@quality}/#{File.basename(file)}"

  img = Magick::Image.read(file).first
  img.write(path)
  img.write(path_uncompressed)

  GC.start

  `#{command_path("jpegoptim")} -f -m#{@quality.to_i} --strip-all -v #{path}`
end

def create_uncompressed_versions
  files = Dir.glob "#{@directory}/*.{jpg}"

  files.each do |file|

    puts "Creating big and thumb uncompressed versions for #{file}..."
    _create_uncompressed_versions(file)
  end
end

def create_versions
  files = Dir.glob "#{@directory}/full_#{@quality}/*.{jpg}"

  files.each do |file|

    puts "Creating big and thumb versions for #{file}..."
    _create_versions(file)
  end
end

def create_full
  files = Dir.glob "#{@directory}/*.{jpg}"
  puts "Will update #{files.size} images..."

  files.each do |file|

    puts "Creating full version for #{file}..."
    _create_full(file)
  end
end

def check_folders
  ([
    "full_#{@quality}", "full_uncompressed_#{@quality}", "big_#{@quality}", \
    "big_uncompressed_#{@quality}", "thumb_#{@quality}", \
    "thumb_uncompressed_#{@quality}"]).each do |name|
    check_folder(name)
  end
end

if (ARGV.length == 0)
  puts "specify desired quality of compression. no more"
  exit 0
end

if !(ARGV[0].to_i > 0)
  puts "desired quality of compression should be a positive number"
  exit 0
end

@directory = File.expand_path File.dirname(__FILE__) + '/images'
@quality = ARGV[0]

check_folders()
create_full()
create_versions()
create_uncompressed_versions()
