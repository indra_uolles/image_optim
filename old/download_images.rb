require "open-uri"

if (ARGV.length == 0)
  puts "specify file name with image urls"
  exit 0
end

File.open(ARGV[0], 'r') do |f1|
  urls = f1.gets.split(",")
  i = 1
  urls.each do |url|
    puts "working with #{url}..."
    open(url) do |f|
      File.open("pic#{i}.jpg","wb") do |f3|
        IO.copy_stream(f, f3)
      end
    end
    i = i + 1
  end
end

f1.close
