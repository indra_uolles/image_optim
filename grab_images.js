var request = require('request'),
    images = require('./images.json'),
    fs = require('fs');

function downloadAndSave(url, i) {

  request.get({url: url, encoding: 'binary'}, function (err, response, body) {
    if (!err && response.statusCode == 200) {
      saveFile(body, "./images/pic" + (i + 1) + ".jpg")
    } else {
      console.log("Error downloading file: " + i)
    }
  });

}

function saveFile(body, name) {

  fs.writeFile(name, body, 'binary', function(err) {

    if (err) {
      console.log(err);
    } else {
      console.log("The file was saved!");
    }

  });

}

images.forEach(downloadAndSave);
